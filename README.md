# Tidsbanken Case

## Table of contents

- [Background](#background)
- [Deployment](#deployment)
- [Planning](#planning)
- [Install](#install)
- [Usage](#install)
- [Keycloak Configuration](#keycloak-configuration)
- [Keycloak JavaScript Adapter](#keycloak-javascript-adapter)
- [Axios Interceptor](#axios-interceptor)
- [Dependencies](#dependencies)
- [Authors](#authors)


## Background

This repository is a solution delivered by the authors as a case project with regards to an assignment given by the Experis Academy - Noroff upskill period.

#

The Tidsbanken Case : 

#
Tidsbanken have approached the candidates and requested that a solution for managing
requests for vacation time for their employees. The application serves a dual purpose.
Firstly, it serves to manage the request and approval process of vacation days for employees and ease the burden of communication. Secondly, the application serves to inform
employees about each other’s approved vacation days so that teams may adequately plan
for periods of absence.

#

The case assignment brief states that the web-application should satisfy the following requirements: 
#

• A static, single-page, front-end using a modern web framework.

• A RESTful API service, through which the front end may interact with the database.

• A suitable database.

## Deployment
Our application is deployed using Azure for our Back-end DB and Vercel for the FE.

## Planning

Our planned design and views of our application can be viewed interactively through this figma link
- Figma link includes DB UML design and DB flow charts.

- All view designs as planner in Week 1 of our development.

- Views created for both administrators and Employees.


<a href="https://www.figma.com/file/8ayrkDCKuqeLKFGuEBBUJg/Tidsbanken?node-id=0%3A1&t=Ax3Gyue1aaJlPfaX-1">Figma </a>


## Install

Using npm

```sh
npm install

npm install --save keycloak-js @react-keycloak/web react-router-dom

npm install react-calendar

npm install sass

npm install axios

npm install user-context -S

npm install -D tailwindcss

npx tailwindcss init

npm install @headlessui/react

npm install react-icons --save

# Install the project dependencies
```



## Usage

```sh
npm start
# Start the React server
```



## Keycloak - Protected Routes (Roles)

Keycloak provides the ability to setup roles for users. The KeycloakRoute Higher Order Component.

```jsx
/* File: src/App.jsx
 * Add a Protected Route using React Router 6.x */
<Routes>
    <Routes path="/profile" 
        element={ 
            <KeycloakRoute role="USER">
                <ProfilePage />
            </KeycloakRoute> 
        }
    />
</Routes>
```

```jsx
// File: routes/KeycloakRoute.jsx
import { Navigate } from "react-router-dom";
import keycloak from "../keycloak";

function KeycloakRoute({ children, role, redirectTo = "/" }) {
  
  if (!keycloak.authenticated) {
    return <Navigate replace to={redirectTo} />;
  }

  if (keycloak.hasRealmRole(role)) {
    return <>{children}</>;
  }

  return <Navigate replace to={redirectTo} />;
}

export default KeycloakRoute;
```

## Connect to Back-End

<a href="https://gitlab.com/tidsbanken-case/backend_case">Backend repository</a>

Make sure you have docker installed and run the following commands in the terminal to locally host the API:
```cmd
cd back-end-repository

docker compose up
```

The API will then be hosted on 

## Authors
#
- <a href="https://www.linkedin.com/in/h%C3%A5kon-solheim-379057175/">Håkon Natland Solheim</a>

- <a href="https://www.linkedin.com/in/andreasnn/">Andreas Navarro Nes</a>

- <a href="https://www.linkedin.com/in/kvernhaug/">Andreas Lysaker Kvernhaug</a>
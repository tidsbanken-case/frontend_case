import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import App from "./App";
import Loading from "./components/loading/Loading";
import KeyCloakService from "./keycloak";
import { BrowserRouter } from "react-router-dom";
import AppContext from "./contexts/AppContext";
const root = ReactDOM.createRoot(document.getElementById("root"));

// Display a loading screen when connecting to Keycloak
root.render(<Loading message="Connecting to Keycloak..." />)

// Initialize Keycloak
const renderApp = () =>
  root.render(
    <BrowserRouter>
      <AppContext>
        <App />
      </AppContext>
    </BrowserRouter>
  );

KeyCloakService.CallLogin(renderApp);

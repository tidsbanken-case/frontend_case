import { useState, useEffect } from "react";
import Calendar from "react-calendar";
import "../sassStyles/Calendar.scss";
import { useUser } from "../contexts/userContext";
import GetIneligiblePeriods from "../fetchFromAPI/ineligiblePeriod/FetchIneligiblePeriod";
import IneligiblePeriodListView from "../components/ineligiblePeriod/ineligiblePeriodListView";
import GetMyUsers from "../fetchFromAPI/user/FetchUser";
import RequestFormOnlyIfEligible from "../components/vacationRequests/RequestFormOnlyIfEligible"

function CalendarAsset() {
  const [date, setDate] = useState(new Date());
  const {ineligiblePeriods} = useUser();

  return (
    <div>
      <GetMyUsers />
      <h1 className="text-center text-2xl font-bold">Make a request</h1>
      <div className="Calendar">
        <div>
          <Calendar
            className={["c1", "c2"]}
            onChange={setDate}
            value={date}
            selectRange={true}
          />
        </div>
        {date.length > 0 ? (
          <>
            <br />
            <p><b>Length of selected vacation:</b> {Math.round(((date[1].getTime() - date[0].getTime()) / (1000 * 60 * 60 *24)))} days.</p>
              {/*TODO: finne ut - finnes det en funksjon som gjoer detta for oss? */}
          </>
        ) : (
          <p>
          </p>
        )}
        <div>
          <section>
              {/* Sende date[0], date[1], tittel, ev. annet til API. 
              load all vacations from api
              REGLER:
                1: Laste inn ALLE requests for innlogget bruker, ev. i forskjellige farger.
                2: Laste inn alle andres GODKJENTE requests
                3: KUN ADMINS: laste inn alt
                spm: Alle godkjente boer lagres i felles liste tilgjengelig for alle, men hvor boer
                hver enkelt brukers ikke godkjente requests lagre - kanskje paa samme plass som brukerens oevrige data??
                Forslag til DB Struktur:
                1: Hver bruker har en egen liste med alle egne requests
                2: Admin skal ha en liste med alt som ikke er godkjent, naar bruker submitter blir det kopiert dit
                  ?: Egne liste for hver type status eller bare filtrere paa et status-parameter
                3: Naar admin godkjenner, flytte til felles aapen liste med alt som er godkjent
              VIKTIG: dette staar sikkert spesifisert i case-teksten.
              Ha en separat metode (utils????) som beregner lengde, den kan kalles ved behov */}
          </section>
        </div>
      </div>
      <br />
      <div>
      <GetIneligiblePeriods/>
        <span className="font-bold">Ineligible Periods:</span>
        <IneligiblePeriodListView ineligiblePeriods={ineligiblePeriods} />
      </div>
      <br />
      {date.length > 0 ? (
        <div>
            <RequestFormOnlyIfEligible periodToCheckStart={date[0]} periodToCheckEnd={date[1]} />
          </div>
        ) : (
          <span className="font-bold text-blue-900">🛈 To continue, please select start and end dates for your request.</span>
      )}
      <br/>
    </div>
  );
}

export default CalendarAsset;

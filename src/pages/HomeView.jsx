import React from "react";
import GetMyUsers from "../fetchFromAPI/user/FetchUser";
import { Link } from "react-router-dom";
import KeyCloakService from "../keycloak";
function HomeView() {

  return (
    <div>
      <GetMyUsers />
      <div className="text-center p-1 pt-2 h-full">
        <div
          id="menu"
          className="flex gap-2 flex-row text-gray-50 relative items-stretch top-3 h-full"
        >
          <Link className={`${KeyCloakService.isAdmin()? 'basis-1/4':'basis-1/3'} relative p-3`} to="/requests">
            <b className="absolute top-9 left-12 text-2xl drop-shadow-md z-20">Vacation Requests</b>
            <img
              id="hover-img-1"
              className="h-full rounded-2xl object-cover hover:scale-105 ease-in-out duration-500"
              src="\images\father_and_daughter.jpg"
              alt="Vacation Requests"
            ></img>
          </Link>
          <Link className={`${KeyCloakService.isAdmin()? 'basis-1/4':'basis-1/3'} relative p-3`} to="/calendar">
            <b className="absolute top-9 left-20 text-2xl drop-shadow-md z-20">Make Request</b>
            <img
              id="hover-img-2"
              className="h-full rounded-2xl object-cover hover:scale-105 ease-in-out duration-500"
              src="\images\friend_group_1.jpg"
              alt="Calendar Dashboard"
            ></img>
          </Link>
          <Link className={`${KeyCloakService.isAdmin()? 'basis-1/4':'basis-1/3'} relative p-3`} to="/profile">
            <b className="absolute top-9 left-24 text-2xl drop-shadow-md z-20">My Profile</b>
            <img
              id="hover-img-3"
              className="h-full rounded-2xl object-cover hover:scale-105 ease-in-out duration-500"
              src="\images\man_using_phone.png"
              alt="Calendar Dashboard"
            ></img>
          </Link>
          {KeyCloakService.isAdmin() === true && (
          <Link className="basis-1/4 relative p-3" to="/adminarea">
            <b className="absolute top-9 left-12 text-2xl drop-shadow-md z-20">Administrator Area</b>
            <img
              id="hover-img-4"
              className="h-full rounded-2xl object-cover hover:scale-105 ease-in-out duration-500"
              src="\images\man_with_laptop.jpg"
              alt="Administrator Area"
            ></img>
          </Link>
          )}
        </div>
      </div>
      <br/>
    </div>
  );
}

export default HomeView;

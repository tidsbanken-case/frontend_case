import KeyCloakService from "../keycloak";
import { useUser } from "../contexts/userContext";
import CreateIneligiblePeriod from "../components/adminArea/createIneligiblePeriod";
import GetMyUsers from "../fetchFromAPI/user/FetchUser";
import VacationRequestFormListView from "../components/vacationRequests/vacationRequestFormListView";
import GetVacationRequests from "../fetchFromAPI/vacationRequests/FetchVacationRequests";

function AdminArea() {
  const { othersPrivateRequests } = useUser();

  const gotoAccountSettings = () => {
    KeyCloakService.MakeUser();
  }
  return (
    <div>
      <GetMyUsers />
      <GetVacationRequests />
      <h1 className="text-center text-2xl font-bold p-2">Admin area</h1>
      <br />
      {/*<br />
      <br />
      <hr className="h-1  my-2 bg-gray-100 border-0 rounded dark:bg-gray-700" />
      <br />*/}
      {KeyCloakService.KEY_CLOAK_ROUTE && (
        <>
          {/*<div className=" relative w-full justify-center items-center">
            <button className=" relative  hovering-button" onClick={gotoAccountSettings}>Create new User</button>
        </div>*/}

          <section className="relative justify-center text-center p-5">
          <h2 className="font-bold text-center text-2xl p-2">Requests</h2>
          <hr className="h-1  my-2 bg-gray-100 border-0 rounded dark:bg-gray-700" />
          <div className="p-2">
            <VacationRequestFormListView
              vacationRequests={othersPrivateRequests}
            />
            {/*TODO: Hvordan hente inn othersPrivateRequests, bare naar man er admin?*/}
          </div>
          <br />
          </section>

          <div>
            <h2 className="font-bold text-center text-2xl p-2">Users:</h2>
            <hr className="h-1  my-2 bg-gray-100 border-0 rounded dark:bg-gray-700" />
            {/*TODO: implementere.*/}
          </div>
          <br />
          <div className="p-2">
            <h2 className="font-bold">Create Ineligible Period:</h2>
            <CreateIneligiblePeriod />
          </div>
        </>
      )}
      <br/>
    </div>
  );
}
export default AdminArea;

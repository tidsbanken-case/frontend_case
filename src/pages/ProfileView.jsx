import ProfileActions from "../components/Profile/ProfileActions";
import UserRequestFormListView from "../components/vacationRequests/userRequestFormListView";
import { useUser } from "../contexts/userContext";
import GetMyUsers from "../fetchFromAPI/user/FetchUser";
import GetVacationRequests from "../fetchFromAPI/vacationRequests/FetchVacationRequests";
function ProfilePage() {
  const { id, email, picture, userRequests } = useUser();
  return (
    <>
      <GetMyUsers />
      <GetVacationRequests />
      <section className="relative">
        <h1 className="text-center text-2xl font-bold">Profile Page</h1>
      
        <hr className="h-1  my-2 bg-gray-100 border-0 rounded dark:bg-gray-700" />
        <br />
        <section className="absolute right-40 top-20">
          <p>E-mail address: {email}</p>
          <hr className="h-0.5 w-64  my-2 bg-gray-100 border-0 rounded dark:bg-gray-700" />
          <p>User ID: {id}</p>
          <hr className="h-0.5 w-16  my-2 bg-gray-100 border-0 rounded dark:bg-gray-700" />
          <p>Current Picture:</p>
          <hr className="h-0.5 w-28  my-2 bg-gray-100 border-0 rounded dark:bg-gray-700" />
          <img
            className="rounded-full w-56 p-2 "
            src={'/images/' + picture}
            alt="Profile"
          ></img>
        </section>
        
        <section className="absolute top-20 left-40">
          <ProfileActions />
        </section>
        <section className="relative justify-center text-center p-5 top-96 ">
        <hr className="h-1  my-2 bg-gray-100 border-0 rounded dark:bg-gray-700" />
          <h2 className="font-bold p-2">My Requests</h2>
          <div className="p-2">
            <UserRequestFormListView vacationRequests={userRequests} />
          </div>
          <GetVacationRequests />
        </section>
      </section>
      <br/>
    </>
  );
}
export default ProfilePage;

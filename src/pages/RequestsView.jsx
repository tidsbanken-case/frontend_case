
import RequestHistory from "../components/vacationRequestHistory/RequestHistory";
import VacationRequestListView from "../components/vacationRequests/vacationRequestListView";
import { useUser } from "../contexts/userContext";
import GetVacationRequests from "../fetchFromAPI/vacationRequests/FetchVacationRequests";
import KeyCloakService from "../keycloak";
import GetMyUsers from "../fetchFromAPI/user/FetchUser";

function RequestsPage() {
  const { othersPublicRequests, othersPrivateRequests, declinedRequests, ownRequests } = useUser();
  return (
    <section className="relative justify-center text-center p-5">
      <h1 className="text-center text-2xl font-bold">Vacation Requests</h1>
      <br />
      <div>
        <GetMyUsers/>
        <GetVacationRequests />
        <br />
        <br />
        <p><b>Approved Requests</b></p>
        <div className="text-green-600">
          <VacationRequestListView vacationRequests={othersPublicRequests} />
        </div>
        <br />
        <p><b>Private Requests</b></p>
        <div className="text-orange-500">
          {KeyCloakService.isAdmin() === true && (
            <VacationRequestListView vacationRequests={othersPrivateRequests} />
            )}
        </div>
        <br />
        <p><b>Rejected Requests</b></p>
        <div className="text-red-700">
          {KeyCloakService.isAdmin() === true && (
            <VacationRequestListView vacationRequests={declinedRequests} />
          )}
        </div>
        <RequestHistory />
      </div>
      <br/>
    </section>
  );
}

export default RequestsPage;

import Keycloak from "keycloak-js";
import { Navigate } from "react-router-dom";
import { ROLES } from "./const/roles";


const keycloak = new Keycloak("/keycloak.json");

const Login = onAuthenticatedCallback => {
  const initoptions = {
    onLoad: "login-required",
    pkceMethod: "S256",
    checkLoginIframe: false
  }
  keycloak
    .init(initoptions)
    .then(function (authenticated) {
      authenticated ? onAuthenticatedCallback() : alert("non authenticated")
    })
    .catch(e => {
      console.dir(e)
      console.log(`keycloak init exception: ${e}`)
    })
}


function KeycloakRoute({ children, role, redirectTo = "/" }) {

  if (!keycloak.authenticated) {
    return <Navigate replace to={redirectTo} />;
  }

  if (keycloak.hasRealmRole(role)) {
    return <>{children}</>;
  }

  return <Navigate replace to={redirectTo} />;
}




const UserName = () => keycloak.tokenParsed.preferred_username;
const firstName = () => keycloak.tokenParsed.given_name;
const lastName = () => keycloak.tokenParsed.family_name;
const Id = () => keycloak.tokenParsed?.sub;
const email = () => keycloak.tokenParsed.email;
const password = '7DD5C902ssslCiWgBGYi';
const Logout = () => keycloak.logout();
const checkToken = () => keycloak;
const isAdminCheck = () => keycloak.hasRealmRole(ROLES.Admin);
const accountSettings = () => keycloak.accountManagement()


const KeyCloakService = {
  CallLogin: Login,
  MakeUser: accountSettings,
  tokenPls: checkToken,
  getId: Id,
  isAdmin: isAdminCheck,
  KEY_CLOAK_ROUTE: KeycloakRoute,
  getUserName: UserName,
  getFirstName: firstName,
  getLastName: lastName,
  getEmail: email,
  getPassword: password,
  LogOut: Logout
}
export default KeyCloakService
/*
    firstname: myToken.given_name,
    lastName: myToken.family_name,
    username: myToken.preferred_username,
    email: myToken.email,
    password: '7DD5C902ssslCiWgBGYi',
        */

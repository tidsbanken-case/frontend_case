import { Route, Routes } from "react-router-dom";
import HomeView from "./pages/HomeView";
import CalendarAsset from "./pages/Calendar";
import ProfileView from "./pages/ProfileView";
import AdminAreaView from "./pages/AdminAreaView";
import MenuNavbar from "./components/navbar/MenuNav";
import { ROLES } from "./const/roles";
import RequestsView from "./pages/RequestsView";
import KeyCloakService from "./keycloak";
import Navbar from "./components/navbar/Navbar";
import GetMyUsers from "./fetchFromAPI/user/FetchUser";

function App() {
  return (
    <>
      <GetMyUsers />
      <MenuNavbar />
      <Navbar />
      <main className="container">
        <Routes>
          <Route path="/" element={<HomeView />} />
          <Route path="/Calendar" element={<CalendarAsset />} />
          <Route path="/Requests" element={<RequestsView />} />
          <Route path="/Profile" element={<ProfileView />} />
          <Route
            path="/AdminArea"
            element={
              <KeyCloakService.KEY_CLOAK_ROUTE role={ROLES.Admin}>
                <AdminAreaView />
              </KeyCloakService.KEY_CLOAK_ROUTE>
            }
          />
        </Routes>
      </main>
    </>
  );
}

export default App;

import { useUser } from "../../contexts/userContext";
import GetMyUsers from "../../fetchFromAPI/user/FetchUser";

function Navbar() {
  const { name, picture } = useUser();

  return (
    <nav>
      <GetMyUsers />
      <div className="container">
        <div className="navbar">
          <ul className="flex flex-row">
            <li>
              <a href="/">
                <img
                  className="text-black basis-1/4 pl-5 flex-none w-28 h-28"
                  src="\images\logo_tidsbanken.png"
                  alt="tidsbanken_LOGO"
                />
              </a>
            </li>
            <div className="text-center h-full p-6 basis-1/3 flex-grow">
              <b className="text-2xl h-full"> Welcome Back, {name}!</b>
              {console.log(name)}
            </div>
            <div className="flex-none w-36">
              {console.log(picture)}
              
              <img
                className="rounded-full w-20 relative place-content-center mx-auto shadow-md shadow-slate-700"
                src={'/images/' + picture}
                alt="userPhoto"
              />
              <p className="text-center">{name}</p>
              <hr className=" h-0.5  my-2 bg-gray-100 border-0 rounded dark:bg-gray-700 " />
              <p className="text-center">Account Manager</p>
              <hr className="h-0.5  my-2 bg-gray-100 border-0 rounded dark:bg-gray-700" />
            </div>
          </ul>
        </div>
      </div>
    </nav>
  );
}
export default Navbar;

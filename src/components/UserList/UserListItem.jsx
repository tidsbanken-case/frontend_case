const UserListItem = ({ element }) => {
    return (
      <li>
        {" "}
        {" User: " +
          element.name +
          " " +
          element.email +
          " Status : " +
          element.admin +
          "  - Comments : " +
          element.vacationRequests}
          {" "}
      </li>
    );
  };
  export default UserListItem;
  
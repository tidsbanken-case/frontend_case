import UserListItem from "./UserListItem"
const UserListView = ({ users }) => {

    let userList = []
    
    if (users !== "") { // TODO: er det tom streng vi skal sjekke etter?
        userList = users
        console.log(users)
        .map((element, index) => <UserListItem key={index + ': ' + element} element={element} />) // TODO: sjekke denna linja
    }

    return (
        <section>
            <ul>
                {userList}
                { !userList.length && // If list is empty...
                    <p className="font-bold text-blue-900">🛈 No Users.</p>
                }
            </ul>
        </section>
    )
}
export default UserListView
import IneligiblePeriodListItem from "./ineligiblePeriodListItem";

const IneligiblePeriodListView = ({ ineligiblePeriods }) => {
    let ineligiblePeriodList = [];
    if (ineligiblePeriods != "") {
        ineligiblePeriodList = ineligiblePeriods.map((element, index) => (
            <IneligiblePeriodListItem
                key={index + ": " + element}
                element={element}
            />
        ));
    }
    return (
        <section>
            <ul>
                {ineligiblePeriodList}
                {!ineligiblePeriodList.length && <p>🛈 No ineligible periods.</p>}
            </ul>
        </section>
    );
};
export default IneligiblePeriodListView;

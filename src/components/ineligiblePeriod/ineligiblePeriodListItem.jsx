const IneligiblePeriodListItem = ({ element }) => {
    return <li>{element.periodStart + "  -  " + element.periodEnd}</li>;
};
export default IneligiblePeriodListItem;

//import { useState } from "react";

import { useUser } from "../../contexts/userContext";

const RequestHistory = () => {
  const {name} = useUser();
  //const [totalNumberOfDays, setTotalNumberOfDays] = useState("")
  //const [daysRemaining, setDaysRemaining] = useState("")

  return (
    <>
      <br />
      <br />
      <br />
      <b>Vacation Information: {name} </b>
      <p>Total number of vacation days allowed: 25{/*{daysRemaining}*/}</p>
      <p>Vacation days remaining: 17</p>
    </>
  );
};
export default RequestHistory;

import { useState } from "react";
import { useUser } from "../../contexts/userContext";
import PatchMyUsers from "../../fetchFromAPI/user/PatchUser";
import KeyCloakService from "../../keycloak";

const apiURL = process.env.REACT_APP_API_URL;
const apiKEY = process.env.REACT_APP_API_KEY;

const ProfileActions = () => {
  const {id, setId, name, setName, email, setEmail, picture, setPicture, admin} = useUser();
  const [password, setPassword] = useState()

  const handlePatchClick = (newEmail) => {
    // TODO.
    const patchObject = {
      "id": id,
      "name": name,
      "email": newEmail,
      "picture": picture,
      "admin": admin
    }
    PatchMyUsers(patchObject)
  }

  const gotoAccountSettings = () => {
    KeyCloakService.MakeUser();
  }

  return (
    <div>
      <b>Change E-mail</b>
      <br />
      <br />
      <form>
        <input
          className="border-red-100 border-2 p-1"
          type={"text"}
          value={email}
          onChange={(e) => setEmail(e.target.value)}
          placeholder={email}
        />
      </form>
      <br />
      <form action="">
        <input 
        className="submit" 
        type="submit" 
        value="Change Profile Picture" />
      </form>
      <br />
      <b>Change password</b>
      <br />
      <form>
        <input
          className="border-red-100 border-2 p-1"
          type={"text"}
          value={password}
          onChange={(e) => setPassword(e.target.value)}
          placeholder="Password"
        />
      </form>
      <button className="border-2 bg-slate-500 rounded-lg p-1" onClick={handlePatchClick}>Submit Changes</button>
      <br/>
      <br/>
      <div className=" relative w-full justify-center items-center">
        <button className=" relative  hovering-button" onClick={gotoAccountSettings}>All account settings</button>
      </div>
    </div>
  );
};
export default ProfileActions;

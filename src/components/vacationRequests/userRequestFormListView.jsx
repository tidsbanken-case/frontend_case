import GetVacationRequests from "../../fetchFromAPI/vacationRequests/FetchVacationRequests"
import UserRequestFormItem from "./userRequestFormItem"



const UserRequestFormListView = ({ vacationRequests }) => {

    let vacationRequestList = []
    
    if (vacationRequests !== "") { // TODO: er det tom streng vi skal sjekke etter?
        vacationRequestList = vacationRequests
        .map((element, index) => <UserRequestFormItem key={index + ': ' + element} element={element} />) // TODO: sjekke denna linja
    }

    return (
        <section>
            <GetVacationRequests />
            <ul> 
                {vacationRequestList}
                { !vacationRequestList.length && // If list is empty...
                    <p>No vacation requests.</p>
                }
            </ul>
        </section>
    )
}
export default UserRequestFormListView;
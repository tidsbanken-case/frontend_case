import axios from "axios";
import { useState } from "react";
import { useUser } from "../../contexts/userContext";

const apiURL = process.env.REACT_APP_API_URL + "vacation_request";

const UserRequestFormItem = ({ element }) => {
    const [title, setTitle] = useState(element.title);
    const [periodStart, setPeriodStart] = useState(element.periodStart);
    const [periodEnd, setPeriodEnd] = useState(element.periodEnd);
    const { id } = useUser();

    const updateVacationRequest = (e) => {
        axios
            .patch(apiURL + "/" + element.id, {
                id: element.id,
                admin: id,
                user: element.user,
                title: title,
                periodStart: periodStart,
                periodEnd: periodEnd,
                status: element.status,
            })
            .then((res) => console.log("Patching vacation request ", res))
            .catch((error) => console.log(error));
        alert("Vacation Request Updated");
        setTitle("");
        setPeriodStart("");
        setPeriodEnd("");
    };

    if (element.status === "PENDING") {
        return (
            <div className="m-4 px-10 py-5 shadow-md rounded-xl bg-orange-100">
                <li className="text-orange-500">
                    <p>
                        <b>User: </b> {element.user}
                    </p>
                    <p>
                        <b>Title: </b> {element.title}
                    </p>
                    <p>
                        <b>Period: </b> {element.periodStart} -{" "}
                        {element.periodEnd}
                    </p>
                    <p>
                        <b>Status: </b> {element.status}
                    </p>
                    <form
                        onSubmit={updateVacationRequest}
                        className="text-black"
                    >
                        <label>Title: </label>
                        <input
                            type="text"
                            value={title}
                            onChange={(e) => setTitle(e.target.value)}
                        />
                        <label>Period Start: </label>
                        <input
                            type="text"
                            value={periodStart}
                            onChange={(e) => setPeriodStart(e.target.value)}
                        />
                        <label>Period End: </label>
                        <input
                            type="text"
                            value={periodEnd}
                            onChange={(e) => setPeriodEnd(e.target.value)}
                        />
                        <button type="submit" className="hovering-button">
                            Update Request
                        </button>
                    </form>
                </li>
            </div>
        );
    }
    if (element.status === "APPROVED") {
        return (
            <div className="m-4 px-10 py-5 shadow-md rounded-xl bg-green-100">
                <li className="text-green-600">
                    <p>
                        <b>User: </b> {element.user}
                    </p>
                    <p>
                        <b>Title: </b> {element.title}
                    </p>
                    <p>
                        <b>Period: </b> {element.periodStart} -{" "}
                        {element.periodEnd}
                    </p>
                    <p>
                        <b>Status: </b> {element.status}
                    </p>
                </li>
            </div>
        );
    }

    return (
        <div className="m-4 px-10 py-5 shadow-md rounded-xl bg-red-100">
            <li className="text-red-700">
                <p>
                    <b>User: </b> {element.user}
                </p>
                <p>
                    <b>Title: </b> {element.title}
                </p>
                <p>
                    <b>Period: </b> {element.periodStart} - {element.periodEnd}
                </p>
                <p>
                    <b>Status: </b> {element.status}
                </p>
            </li>
        </div>
    );
};
export default UserRequestFormItem;

import VacationRequestFormItem from "./vacationRequestFormItem"
// TODO: SLETTE DENNE FILEN, TROR IKKE DEN BRUKES NOE STED



const VacationRequestFormListView = ({ vacationRequests }) => {

    let vacationRequestList = []
    
    if (vacationRequests !== "") { // TODO: er det tom streng vi skal sjekke etter?
        vacationRequestList = vacationRequests
        .map((element, index) => <VacationRequestFormItem key={index + ': ' + element} element={element} />) // TODO: sjekke denna linja
    }

    return (
        <section>
            <ul> 
                {vacationRequestList}
                { !vacationRequestList.length && // If list is empty...
                    <p>No vacation requests.</p>
                }
            </ul>
        </section>
    )
}
export default VacationRequestFormListView
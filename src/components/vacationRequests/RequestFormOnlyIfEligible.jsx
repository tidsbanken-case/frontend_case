import { useUser } from "../../contexts/userContext";
import VacationRequestForm from "./vacationRequestForm";


const RequestFormOnlyIfEligible = ({ periodToCheckStart, periodToCheckEnd }) => {

    // Compares the requested period agains ALL ineligible periods.
    function RequestedPeriodIsEligible() {
        const { ineligiblePeriods } = useUser();

        for (const oneIneligiblePeriod of ineligiblePeriods) {
            if (!ineligiblePeriodDoesNotConflict(oneIneligiblePeriod)) {
                return false
            }
        }
        return true
    }

    // Called by RequestedPeriodIsEligible(), compares the requested period agains a SINGLE ineligible period.
    function ineligiblePeriodDoesNotConflict(ineligibleRange) {
        const ineligiblePeriodStart = new Date(ineligibleRange.periodStart)
        const ineligiblePeriodEnd = new Date(ineligibleRange.periodEnd)

        if ((periodToCheckStart < ineligiblePeriodStart && periodToCheckEnd < ineligiblePeriodStart) ||
            (periodToCheckStart > ineligiblePeriodEnd && periodToCheckEnd > ineligiblePeriodEnd)) {
            return true
        }
        return false
    }

    return (
        <section>
            {RequestedPeriodIsEligible() ? (
                <VacationRequestForm startDate={periodToCheckStart.toISOString().slice(0, 10)} endDate={periodToCheckEnd.toISOString().slice(0, 10)}></VacationRequestForm>
            ) : (
                <span className="font-bold text-blue-900">🛈 The dates you have selected are ineligible. Please select other dates.</span>
            )
            }
        </section>
    )
}

export default RequestFormOnlyIfEligible;

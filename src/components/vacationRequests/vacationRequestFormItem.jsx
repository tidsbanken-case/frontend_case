import axios from "axios";
import { useState } from "react";
import { useForm } from "react-hook-form";
import { useUser } from "../../contexts/userContext";

const apiURL = process.env.REACT_APP_API_URL + "vacation_request";

const VacationRequestFormItem = ({ element }) => {
    const [title, setTitle] = useState(element.title);
    const [periodStart, setPeriodStart] = useState(element.periodStart);
    const [periodEnd, setPeriodEnd] = useState(element.periodEnd);
    const [status, setStatus] = useState("PENDING");
    const { id } = useUser();

    const updateVacationRequest = (e) => {
        if (periodStart === "") {
            setPeriodStart(element.periodStart);
        }
        if (periodEnd === "") {
            setPeriodEnd(element.periodEnd);
        }
        axios
            .patch(apiURL + "/" + element.id, {
                id: element.id,
                admin: id,
                user: element.user,
                title: title,
                periodStart: periodStart,
                periodEnd: periodEnd,
                status: status,
            })
            .then((res) => console.log("Patching vacation request ", res))
            .catch((error) => console.log(error));
        alert("Vacation Request Updated");
        setTitle("");
        setPeriodStart("");
        setPeriodEnd("");
        setStatus("");
    };

    

    if (id !== element.user) {
        return (
            <div className="m-4 px-10 py-5 shadow-md rounded-xl bg-orange-100">
                <li className="text-orange-500">
                    <p><b>User: </b> {element.user}</p>
                    <p><b>Title: </b> {element.title}</p>
                    <p><b>Period: </b> {element.periodStart} - {element.periodEnd}</p>
                    <p><b>Status: </b> {element.status}</p>
                    <form
                        onSubmit={updateVacationRequest}
                        className="text-black"
                    >
                        <label>Title: </label>
                        <input
                            type="text"
                            value={title}
                            onChange={(e) => setTitle(e.target.value)}
                            placeholder={element.title}
                        />
                        <label>Period Start: </label>
                        <input
                            type="text"
                            value={periodStart}
                            onChange={(e) => setPeriodStart(e.target.value)}
                            placeholder={element.periodStart}
                        />
                        <label>Period End: </label>
                        <input
                            type="text"
                            value={periodEnd}
                            onChange={(e) => setPeriodEnd(e.target.value)}
                            placeholder={element.periodEnd}
                        />
                        <label>Status: </label>
                        <select className="px-4 py-3 shadow-md rounded-full" onChange={(e) => setStatus(e.target.value)}>
                            <option value="PENDING">Pending</option>
                            <option value="APPROVED">Approved</option>
                            <option value="REJECTED">Rejected</option>
                        </select>
                        <button type="submit" className="hovering-button">
                            Update Request
                        </button>
                    </form>
                </li>
            </div>
        );
    }

    return (
        <div className="m-4 px-10 py-5 shadow-md rounded-xl bg-orange-100">
            <li className="text-orange-500">
                <p><b>User: </b> {element.user}</p>
                <p><b>Title: </b> {element.title}</p>
                <p><b>Period: </b> {element.periodStart} - {element.periodEnd}</p>
                <p><b>Status: </b> {element.status}</p>
            </li>
        </div>
    );
};
export default VacationRequestFormItem;

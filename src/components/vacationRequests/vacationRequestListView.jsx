import VacationRequestItem from "./vacationRequestListItem"

const VacationRequestListView = ({ vacationRequests }) => {

    let vacationRequestList = []
    
    if (vacationRequests !== "") { // TODO: er det tom streng vi skal sjekke etter?
        vacationRequestList = vacationRequests
        .map((element, index) => <VacationRequestItem key={index + ': ' + element} element={element} />) // TODO: sjekke denna linja
    }

    return (
        <section>
            <ul> 
                {vacationRequestList}
                { !vacationRequestList.length && // If list is empty...
                    <p className="font-bold text-blue-900">🛈 No vacation requests.</p>
                }
            </ul>
        </section>
    )
}
export default VacationRequestListView
const VacationRequestListItem = ({ element }) => {
    let divName = "";

    if (element.status === "PENDING") {
        divName = "m-4 px-10 py-5 shadow-md rounded-xl bg-orange-100";
    } else if (element.status === "APPROVED") {
        divName = "m-4 px-10 py-5 shadow-md rounded-xl bg-green-100";
    } else {
        divName = "m-4 px-10 py-5 shadow-md rounded-xl bg-red-100";
    }

    return (
        <div className={divName}>
            <li>
                <p>
                    <b>User: </b> {element.user}
                </p>
                <p>
                    <b>Title: </b> {element.title}
                </p>
                <p>
                    <b>Period: </b> {element.periodStart} - {element.periodEnd}
                </p>
                <p>
                    <b>Status: </b> {element.status}
                </p>
            </li>
        </div>
    );
};
export default VacationRequestListItem;

import PostMyRequest from "../../fetchFromAPI/vacationRequests/PostVacationRequests"
import { useUser } from "../../contexts/userContext";

const VacationRequestForm = ( {startDate, endDate} ) => {

    const { id } = useUser();

    function handleSubmit(e) {
        e.preventDefault();
        
        
        const form = e.target
        const formData = new FormData(form)
        const formJson = Object.fromEntries(formData.entries());

        alert(`Your request, "${formJson.requestTitle}", has been successfully submitted for review.`)

        const patchObject = {
            "title": formJson.requestTitle,
            "periodStart": startDate,
            "periodEnd": endDate,
            "status": "PENDING",
            "user": id // TODO: passe riktig bruker
        }
        PostMyRequest(patchObject)
        
        
    }
    
    return (
        <form onSubmit={ handleSubmit }>
            <label className="font-bold">
                Request title: <input name="requestTitle" placeholder="Give your request a title" />
            </label>
            <button className="hovering-button" type="submit">Submit request</button>
            {/* TODO: mulighet for aa legge inn kommentarer? */}
        </form>
    )
}
export default VacationRequestForm
import { useState } from "react";
import { useUser } from "../../contexts/userContext";
import axios from "axios";

const apiURL = process.env.REACT_APP_API_URL + "ineligible_period";

const CreateIneligiblePeriod = () => {
    const [periodStart, setPeriodStart] = useState("");
    const [periodEnd, setPeriodEnd] = useState("");
    const { id } = useUser();

    const createPeriod = (e) => {
        e.preventDefault();
        axios
            .post(apiURL, {
                periodStart: periodStart,
                periodEnd: periodEnd,
                creator: id,
            })
            .then((res) => console.log("Posting ineligible period ", res))
            .catch((error) => console.log(error));
        
        alert("Ineligible Period Created")
        // Clear text in form
        setPeriodStart('');
        setPeriodEnd('');
    };

    return (
        <div>
            <form 
            onSubmit={createPeriod}>
                <label Period Start></label>
                <input
                    type="text"
                    value={periodStart}
                    placeholder="Start (ex. 2023-12-30)"
                    onChange={(e) => setPeriodStart(e.target.value)}
                />
                <label Period End></label>
                <input
                    type="text"
                    value={periodEnd}
                    placeholder="End (ex. 2023-12-31)"
                    onChange={(e) => setPeriodEnd(e.target.value)}
                />
                <button type="submit" className="hovering-button">Create Period</button>
            </form>
        </div>
    );
};
export default CreateIneligiblePeriod;

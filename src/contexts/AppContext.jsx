//impl app context --
import UserProvider from "./userContext"
const AppContext = ({ children }) => {
    
    return(
        <UserProvider>
            { children }
        </UserProvider>
    )

}
export default AppContext
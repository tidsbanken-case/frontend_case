import React, { createContext, useContext, useState } from "react";

//Context -> Exposes User(object)
const UserContext = createContext();

export const useUser=() =>  useContext(UserContext); //returns object { user, setUser } etc..

//Provider -> Manages state of Object
const UserProvider = ({ children }) => {
    //All Users
    const [user, setUser] = useState(null);
    const [name, setName] = useState('')
    const [email, setEmail] = useState('')
    const [id, setId] = useState('')
    const [ownRequests, setOwnRequests] = useState([]);
    const [comments, setComments] = useState('')
    const [picture, setPicture] = useState('')
    const [othersPublicRequests, setOthersPublicRequests] = useState([])
    // Only rendered in AdminView
    const [ineligiblePeriods, setIneligiblePeriods] = useState('')
    const [othersPrivateRequests, setOthersPrivateRequests] = useState([])
    const [declinedRequests, setDeclinedRequests] = useState([])
    const [userRequests, setUserRequests] = useState([])
    const [getAllUsersAPI, setAllUsers] = useState([])
    const state = {
        user, //USERNAME
        setUser,
        name, //FullName
        setName,
        email,
        setEmail,
        id,
        setId,
        ownRequests,
        setOwnRequests,
        othersPublicRequests,
        setOthersPublicRequests,
        othersPrivateRequests,
        setOthersPrivateRequests,
        ineligiblePeriods,
        setIneligiblePeriods,
        comments,
        setComments,
        picture,
        setPicture,
        declinedRequests,
        setDeclinedRequests,
        userRequests,
        setUserRequests,
        getAllUsersAPI,
        setAllUsers
    }
    
    return (
        <UserContext.Provider value={state}>
            {children}
        </UserContext.Provider>
    )
}
export default UserProvider
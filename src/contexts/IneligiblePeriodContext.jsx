import { createContext, useContext, useState } from "react";

const IneligiblePeriodListContext = createContext();

export const useIneligiblePeriodList = () =>
    useContext(IneligiblePeriodListContext);

const IneligiblePeriodListProvider = ({ children }) => {
    const [ineligiblePeriods, setIneligiblePeriods] = useState('');

    const state = {
        ineligiblePeriods,
        setIneligiblePeriods,
    };

    return (
        <IneligiblePeriodListContext.Provider value={state}>
            {children}
        </IneligiblePeriodListContext.Provider>
    );
};
export default IneligiblePeriodListProvider;

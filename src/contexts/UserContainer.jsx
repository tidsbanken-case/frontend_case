import React, { useContext } from "react";
import { UserContext } from "./userContext";


//dette fungerer ikke enda, den ber om bruk av alle variablene satt i usercontext
//mulig vi må dele de opp, men den vil heller ikke rendre siden 
//(kan være usercontext må være i homeview og)

//men undersøker Global Context atm (kan være det er en smartere løsning)
const UserContainer = () => {
  const { user, email } = useContext(UserContext);
  return (
    <>
      <p>
        {user} + {email}
      </p>
    </>
  );
};
export default UserContainer;

import axios from "axios";
import { useEffect, useState } from "react";
import { useUser } from "../../contexts/userContext";

let apiURL = process.env.REACT_APP_API_URL;
apiURL += "vacation_request";
const apiKEY = process.env.REACT_APP_API_KEY;

export default function GetVacationRequests() {
    const {
        id,
        setOthersPublicRequests,
        setOthersPrivateRequests,
        setDeclinedRequests,
        setUserRequests,
    } = useUser();
    useEffect(() => {
        GetAllVacationRequests();
    }, []);
    const GetAllVacationRequests = () => {
        axios
            .get(apiURL, {
                headers: {
                    "Access-Control-Allow-Credentials": true,
                    "Access-Control-Allow-Origin": "*",
                    "x-apikey": apiKEY,
                },
                responseType: "json",
                "Access-Control-Allow-Origin": "*",
                "Content-Type": "application/json",
                "x-api-key": apiKEY,
            })
            .then((res) => {
                const approved = "APPROVED";
                const pending = "PENDING";
                const rejected = "REJECTED";

                const approvedRequests = res.data.filter(
                    (res) => res.status === approved
                );
                const pendingResponse = res.data.filter(
                    (res) => res.status === pending
                );
                const rejectedResponse = res.data.filter(
                    (res) => res.status === rejected
                );
                const userRequests = res.data.filter((res) => res.user === id);
                const displayPending = [];
                const displayApproved = [];
                const displayRejected = [];
                const displayUserRequests = [];

                for (let index = 0; index < pendingResponse.length; index++) {
                    displayPending.push(pendingResponse[index]);
                }
                for (let index = 0; index < rejectedResponse.length; index++) {
                    displayRejected.push(rejectedResponse[index]);
                }
                for (let j = 0; j < approvedRequests.length; j++) {
                    displayApproved.push(approvedRequests[j]);
                }
                for (let index = 0; index < userRequests.length; index++) {
                    displayUserRequests.push(userRequests[index]);
                }

                setOthersPrivateRequests(displayPending);
                setOthersPublicRequests(displayApproved);
                console.log(displayRejected);
                setDeclinedRequests(displayRejected);
                setUserRequests(userRequests);
            })
            .catch((error) => console.error(`Error: ${error}`));
    };
}

import axios from "axios";
import { useEffect } from "react";
import { useUser } from "../../contexts/userContext";

const apiURL = process.env.REACT_APP_API_URL + "ineligible_period";

export default function GetIneligiblePeriods() {
    const { setIneligiblePeriods } = useUser();
    useEffect(() => {
        GetAllIneligiblePeriods();
    }, []);
    const GetAllIneligiblePeriods = () => {
        axios
            .get(apiURL, {
                headers: {
                    "Access-Control-Allow-Credentials": true,
                    "Access-Control-Allow-Origin": "*",
                },
                responseType: "json",
                "Access-Control-Allow-Origin": "*",
                "Content-Type": "application/json",
            })
            .then((res) => {
                setIneligiblePeriods(res.data);
                console.log(res.data)
            });
    };
}

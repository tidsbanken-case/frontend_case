import axios from "axios";
import React, { useEffect } from "react";
import { useUser } from "../../contexts/userContext";
import KeyCloakService from "../../keycloak";
let apiURL = process.env.REACT_APP_API_URL;
apiURL += "users";
const apiKEY = process.env.REACT_APP_API_KEY;

export default function GetMyUsers() {
  const {
    setUser,
    setName,
    setEmail,
    setId,
    setOwnRequests,
    setOthersPublicRequests,
    setOthersPrivateRequests,
    setIneligiblePeriods,
    setComments,
    setPicture,
    setAllUsers,
  } = useUser();
  useEffect(() => {
    GetAllUsers();
  }, []);


  const GetAllUsers = () => {
    axios
      .get(apiURL, {
        headers: {
          "Access-Control-Allow-Credentials": true,
          "Access-Control-Allow-Origin": "*",
          "x-apikey": apiKEY,
        },
        responseType: "json",
        "Access-Control-Allow-Origin": "*",
        "Content-Type": "application/json",
        "x-api-key": apiKEY,
      })
      .then((res) => {
        const keycloakID = KeyCloakService.getId();
        let getAllUsers = res.data[0]
        let UserResponse = res.data.filter((res) => res.keycloakId === keycloakID);
        UserResponse = UserResponse[0];
        setUser(UserResponse);
        setEmail(UserResponse.email);
        setId(UserResponse.id);
        setName(UserResponse.name);

        if (UserResponse.name === "Nick"){
          setPicture("man_smiling_1.jpg")
        } if(UserResponse.name === "Jones"){
          setPicture("man_bw_1.jpg")
        }
        setOwnRequests(UserResponse.vacationRequests);
        setComments(UserResponse.comments);
        //setIneligiblePeriods(UserResponse.createdIneligiblePeriods); // kommentert ut for å kunne vise ineligible periods i CalendarView
        setAllUsers(getAllUsers);
        
      })
      .catch((error) => console.error(`Error: ${error}`));
  };
}

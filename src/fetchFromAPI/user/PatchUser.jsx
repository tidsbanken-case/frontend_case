import axios from "axios";
import { useUser } from "../../contexts/userContext";

let apiURL = process.env.REACT_APP_API_URL
const apiKEY = process.env.REACT_APP_API_KEY;

export default function PatchMyUsers(patchObject) {
  const { id , setId, email, setEmail,picture, setPicture} = useUser() 
  
  
  axios.patch(
    `${apiURL}users/1`,
    patchObject,
    {'x-apikey': apiKEY }
        ).then((res) => {
            // TODO: how to handle response?
        })
        .catch(error => console.error(`Error: ${error}`))
      }